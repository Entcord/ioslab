//
//  Person.swift
//  Task1
//
//  Created by Vadzim Petrusenka on 2/22/20.
//  Copyright © 2020 Vadzim Petrusenka. All rights reserved.
//

import Foundation

struct Person: CustomStringConvertible{
    var firstName: String
    var secondName: String
    var birthdate: String
    var family: Family
    
    var fullname: String{
        return firstName + secondName
    }
    
    init(firstName: String, secondName: String, birthdate: String, family: Family){
        self.firstName = firstName
        self.secondName = secondName
        self.birthdate = birthdate
        self.family = family;
    }
    
    var description: String {
        return "First name: \(firstName), Second name: \(secondName), Full name: \(fullname), birthdate: \(birthdate), family: \(family)"
    }
}
