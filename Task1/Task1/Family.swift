//
//  Family.swift
//  Task1
//
//  Created by Vadzim Petrusenka on 2/22/20.
//  Copyright © 2020 Vadzim Petrusenka. All rights reserved.
//

import Foundation

class Family: CustomStringConvertible{
    var address: Address
    var members: [Person] = []
    
    init(address: Address){
        self.address = address
    }
    
    var description: String{
        return "Address: \(address), Members: \(members)"
    }
    
    func addMember(member: Person){
        members.append(member)
    }
}
