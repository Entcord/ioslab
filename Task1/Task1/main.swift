//
//  main.swift
//  Task1
//
//  Created by Vadzim Petrusenka on 2/22/20.
//  Copyright © 2020 Vadzim Petrusenka. All rights reserved.
//

import Foundation

print("start")

let smithAddress: Address = Address(country: "Belarus", city: "Gomel", street: "60 let SSSR", zip: 214056)
let berkAddress: Address = Address(country: "USA", city: "Chicago", street: "202 avenue", zip: 123456)

let smith: Family = Family(address: smithAddress)
let berk: Family = Family(address: berkAddress)

let alex: Person = Person(firstName: "Alex", secondName: "Smith", birthdate: "12.02.1970", family: smith)
let bob: Person = Person(firstName: "Bob", secondName: "Smith", birthdate: "12.02.1940", family: smith)
let jane: Person = Person(firstName: "Jane", secondName: "Smith", birthdate: "12.02.1937", family: smith)

let peter: Person = Person(firstName: "Peter", secondName: "Berk", birthdate: "12.02.1967", family: berk)
let richard: Person = Person(firstName: "Richard", secondName: "Berk", birthdate: "12.02.1941", family: berk)
let mary: Person = Person(firstName: "Mary", secondName: "Berk", birthdate: "12.02.1938", family: berk)

smith.addMember(member: alex)
smith.addMember(member: bob)
smith.addMember(member: jane)

berk.addMember(member: peter)
berk.addMember(member: richard)
berk.addMember(member: mary)

let familyArray: [Family] = [smith, berk]
print(familyArray)

print("end")

