//
//  Address.swift
//  Task1
//
//  Created by Vadzim Petrusenka on 2/22/20.
//  Copyright © 2020 Vadzim Petrusenka. All rights reserved.
//

import Foundation

struct Address: CustomStringConvertible{
    var country: String
    var city: String
    var street: String
    var zip: Int
    
    init(country: String, city: String, street: String, zip: Int){
        self.country = country
        self.city = city
        self.street = street
        self.zip = zip
    }
    
    var description: String{
        return "Country: \(country), City: \(city), Street: \(street), Zip: \(zip)"
    }
}
